package com.project.bli.entity;

public enum RoleName {
	
	ROLE_SUPERADMIN,
	ROLE_ADMIN,
	ROLE_VENDOR,
	ROLE_STAFF
	
}
