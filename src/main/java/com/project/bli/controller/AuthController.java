package com.project.bli.controller;

import java.net.URI;
import java.util.Collections;
import java.util.Date;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.bli.entity.Role;
import com.project.bli.entity.User;
import com.project.bli.model.ApiResponse;
import com.project.bli.model.SignUpRequest;
import com.project.bli.repository.RoleRepository;
import com.project.bli.repository.UserRepository;

@RestController
public class AuthController {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		logger.info("Register User Controller triggered!");
		logger.info("Checking if username is already taken ...");
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}
		logger.info("Username available ...");
		// Creating user's account
		logger.info("Generating new user ...");
		User user = new User();
		user.setUsername(signUpRequest.getUsername());
		// Encode User's password
		user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
		
		logger.info("Checking if role exists ...");
		System.out.println("Role: " + signUpRequest.getRole());
		Role userRole = roleRepository.findByName(signUpRequest.getRole()).get();
		System.out.println("userRole" + userRole);
		
		logger.info("Setting role into user ...");
		user.setRoles(Collections.singleton(userRole));

		logger.info("Trying to save user ...");
		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUsername()).toUri();

		logger.info("Adding new user potentially successful ...");
		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}
	
	@GetMapping("/signup/test")
	public String testController() {
		return "Test Controller Return";
	}
	
	@GetMapping("/staff/active/{nip}")
	public Date getLastActive(@PathVariable String nip) {
		logger.info("Attempting to retrieve staff last active date ...");
		if (userRepository.existsByUsername(nip)) {
			logger.info("Potentially succeed on getting last active date ...");
			return userRepository.getLastActive(nip);
		}
		logger.info("Potentially failed on getting last active date, returning null ...");
		return null;
	}

}
