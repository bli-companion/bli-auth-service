package com.project.bli.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.bli.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findByIdIn(List<Long> userIds);
	Optional<User> findByUsername(String username);
	Boolean existsByUsername(String username);
	
	@Query(
		value="SELECT last_active FROM credential WHERE username=?1",
		nativeQuery=true)
	Date getLastActive(String username);
}
