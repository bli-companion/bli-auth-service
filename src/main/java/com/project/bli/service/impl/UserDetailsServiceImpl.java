package com.project.bli.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.bli.entity.User;
import com.project.bli.filter.JwtUsernameAndPasswordAuthenticationFilter;
import com.project.bli.model.UserPrincipal;
import com.project.bli.repository.UserRepository;

@Component
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(JwtUsernameAndPasswordAuthenticationFilter.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userRepository.findByUsername(username).orElseThrow(
				() -> new UsernameNotFoundException("User not found with username or email : " + username));
		

		logger.info("Attempting to change last active of user: " + user.getUsername() + " ...");
		System.out.println("Name: " + user.getUsername());
		User userUpdate = userRepository.findByUsername(user.getUsername()).get();
		userUpdate.setLast_active(new Date());
		System.out.println("User: " + userUpdate);
//		userRepository.save(user);
		logger.info("Update last active potentially successful ... ");
		
		return UserPrincipal.create(user);
	}
	
	@Transactional
	public UserDetails loadUserById(Long id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + id));
		return UserPrincipal.create(user);
	}

}
